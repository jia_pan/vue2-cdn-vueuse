# VueUse

* 1.什麼是VueUse

* 2.為甚麼要用VueUse

* 3.要怎麼用VueUse

-----

#

## 一、什麼是VueUse?


[VueUse官網](https://vueuse.org/)


>VueUse 是一套 Vue ComposionApI 的常用**工具集**，他的初衷是將一切不支援JS 的API 變得支援響應。
>
>---

## 二、為什麼要用VueUse

### 以實現地理資訊定位為例

#### 不使用VuewUse


Service.js
```javascript
Vuex store

    state: () => ({
        geolocationInfo: null,
    }),
    mutations: {
        setGeolocationInfo(state,payload) {
            state.geolocationInfo = payload
        }
    },
    actions: {
        updateGeolocationInfo(context){
            if ("geolocation" in navigator) {
                navigator.geolocation.getCurrentPosition( position => {
                    context.commit('setGeolocationInfo', position)
                    //console.log(`geolocation is update`)
                })
            } else {
                console.log (`geolocation IS NOT available`)
            }
        }
    },
    getters: {
        /**
         * 使用者定理資訊
         * @param {any} state
         * @returns{Object} GeolocationInfo
         */
        geolocationInfo(state) {
            return state.geolocationInfo
        },
        /**
         * 使用者座標
         * @param {any} state state
         * @returns{[Number,Number]} 座標陣列
         */
        userCoordinate(state) {
            if (!state.geolocationInfo) {
                console.log(`geolocationInfo is un init`)
                return undefined
            }
            console.log(state.geolocationInfo)
            const x = state.geolocationInfo.coords.longitude,
                  y = state.geolocationInfo.coords.latitude
            return [x,y]
        }
    }
}




```

index.js

```javascript


...略

    created() {
        console.log('vue is loading')
        const me = this
        me.$store.dispatch('user/updateGeolocationInfo')

        //定時更新瀏覽者位置資訊
        const tenSecond = 10000,
              source = interval(tenSecond)
        this.geolocationInfoInterval = source.subscribe(() => {
           me.$store.dispatch('user/updateGeolocationInfo')
        })
        console.log(me.store.user.geolocationInfo)

    },



```


使用VuewUse

```javascript

const { coords, locatedAt, error } = useGeolocation()

window.vue = new Vue({
    ...略
    created() {
        const me = this
        console.log(coords)
    },
    ...略
})



```

[useGeolocation](https://vueuse.org/core/usegeolocation/)

### 其他好用功能


[onClickOutside](https://vueuse.org/core/onClickOutside/)


----

### 三.要怎麼用VueUse

#### Vue2 CDN

1.安裝Vue2 composionAPI 擴充套件


```
<script src="https://cdn.jsdelivr.net/npm/@vue/composition-api@1.3.3"></script> 
```
2.安裝VueUse

```
<script src="https://unpkg.com/@vueuse/shared"></script>
<script src="https://unpkg.com/@vueuse/core"></script>
```


3.要用時引入

```javascript
const { useGeolocation } = VueUse
const { coords, locatedAt, error} = useGeolocation()


...略
  created(){
    console.log(coords.value)
    
  },




```
----


[線上測試 Code Pen CDN 環境](https://codepen.io/misooncodepen/pen/OJjQMjq?editors=1010)
